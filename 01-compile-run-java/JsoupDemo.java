import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class JsoupDemo {
    public static void main(String[] args) throws IOException {
        String url = "https://en.wikipedia.org/wiki/Main_Page";
        Document doc = Jsoup.connect(url).get();
        String result = doc.select("#articlecount").get(0).text();
        System.out.println("Wikipedia has " + result);
    }

}
