#!/usr/bin/env bash


# Create a machine called "machine1"
NAME='machine1'

docker-machine create ${NAME}

# Connect to it with:
eval $(docker-machine env ${NAME})

# you may stop, start and restart it
docker-machine restart ${NAME}

# Note that IP may have changed after restart, connect to it again with:
eval $(docker-machine env ${NAME})

# to log in to it
docker-machine ssh ${NAME}

# see which machine is "active", i.e to which you may interact in the current shell

# to find its ip
docker-machine url ${NAME}

# to delete the machine:
docker-machine rm ${NAME}


# Commands used in screencast
docker-machine
docker-machine create machine1
docker-machine env machine1
eval $(docker-machine env machine1)
docker images
docker run hello-world
docker run -p 80:80 -d nginx:alpine
curl localhost
docker-machine ip machine1
curl $(docker-machine ip machine1)
echo "visit $(docker-machine ip machine1) in your browser"


docker-machine active
docker-machine inspect machine1
docker-machine inspect --format "{{ .Driver.DiskSize }}" machine1


docker-machine create machine2
docker-machine env machine2
eval $(docker-machine env machine2)
docker-machine active
docker run --restart always -d postgres:alpine
docker ps
docker-machine stop machine2
docker info
echo "Unset DOCKER_ variables or open a new terminal"
unset DOCKER_HOST DOCKER_MACHINE_NAME DOCKER_CERT_PATH DOCKER_TLS_VERIFY
docker info
docker-machine status machine2
docker-machine start machine2
eval $(docker-machine env machine2)
docker ps

docker-machine ssh
docker-machine create default
docker-machine ssh
uname -a
docker info
docker info | grep CPU
docker ps
pwd
exit

echo "Only local VMs with Docker Machine?"
docker-machine create -d amazonec2 --help
docker-machine create -d amazonec2 --amazonec2-region us-west-2 --amazonec2-vpc-id vpc-5839e121 --amazonec2-subnet-id subnet-9e5960d6 --amazonec2-instance-type t2.medium --amazonec2-open-port 80 machine-on-aws

docker-machine env machine-on-aws
eval $(docker-machine env machine-on-aws)
docker info
docker run -d -p 80:80 httpd
docker-machine ip machine-on-aws
curl `docker-machine ip machine-on-aws`
echo "visit $(docker-machine ip machine-on-aws) in your browser"

docker-machine ls
docker info

docker-machine rm machine-on-aws
aws ec2 describe-instances --region us-west-2 --filters "Name=tag:Name,Values=machine-on-aws"
