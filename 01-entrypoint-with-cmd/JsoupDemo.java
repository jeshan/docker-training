import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.util.Arrays;

public class JsoupDemo {

    public static void main(String[] args) throws IOException {
        System.out.println("Arguments received: " + Arrays.toString(args));
        int index;
        switch(args[0]) {
            case "en":
                index = 0;
                break;
            case "fr":
                index = 1;
                break;
            case "de":
                index = 2;
                break;
            default:
                throw new IllegalArgumentException(args[0]);
        }
        String[] urls = new String[] {"https://en.wikipedia.org/wiki/Main_Page", "https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Accueil_principal", "https://de.wikipedia.org/wiki/Wikipedia:Hauptseite"};

        Document doc = Jsoup.connect(urls[index]).get();
        String[] selectors = new String[] {"#articlecount", "#accueil_2017_contenu > div.portail-droite > div:nth-child(1) > table > tbody > tr:nth-child(2) > td:nth-child(1)", "#hauptseite-willkommen > div > p > a:nth-child(5)"};
        String result = doc.select(selectors[index]).get(0).text();
        System.out.println("Wikipedia has " + result);
    }
}
